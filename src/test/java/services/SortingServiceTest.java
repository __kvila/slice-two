package services;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import static org.junit.jupiter.api.Assertions.*;

class SortingServiceTest {

    private final SortingService cut = new SortingService();

    @Test
    void quickSortingTest() {
        int[] actual = cut.quickSorting(new int[]{1, 2, 3, 2, 22, 10, 17, -5, 0, 4, 8}, 0, 10);
        int[] expected = new int[]{-5, 0, 1, 2, 2, 3, 4, 8, 10, 17, 22};
        assertArrayEquals(expected, actual);
    }

    static Arguments[] quickSortingNullTestArgs() {
        return new Arguments[] {
                Arguments.arguments(new int[]{1, 2, 3, 2, 22, 10, 17, -5, 0, 4, 8}, -1, 10),
                Arguments.arguments(new int[]{1, 2, 3, 2, 22, 10, 17, -5, 0, 4, 8}, 0, 11),
                Arguments.arguments(null, 1, 10)
        };
    }

    @ParameterizedTest
    @MethodSource("quickSortingNullTestArgs")
    void quickSortingNullTest(int[] arr, int low, int high) {
        int[] actual = cut.quickSorting(arr, low, high);
        assertNull(actual);
    }
}